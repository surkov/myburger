import $ from "jquery";
import 'waypoints/lib/jquery.waypoints.min';
import 'jquery.scrollto/jquery.scrollTo.min';
import 'fullpage.js/dist/jquery.fullpage.min';
import 'owl.carousel/dist/owl.carousel.min';


function app() {
  $(document).ready(function() {
    $('.hero-content-wrapper').removeClass('unscrollable');
    $('body').removeClass('unscrollable');

    $('.bottom-section-content-wrapper').scroll(function (event) {
      if (event.currentTarget.scrollTop === 0) {
        $.fn.fullpage.moveTo(4, 0);
      }
    });

    $('.hero-content-wrapper').scroll(function (event) {
      const $mainPhoneBlock = $('.main-phone-block');
      const $subheroPhoneBlock = $('.subhero-phone-block');

      $('.hero-content-wrapper').on('DOMMouseScroll mousewheel', (e) => {
        if (e.originalEvent.wheelDelta < 0) {
          if (e.currentTarget.scrollTop >= this.offsetHeight) {
            $mainPhoneBlock.show();
            $subheroPhoneBlock.hide();
          } else {
            $mainPhoneBlock.hide();
            $subheroPhoneBlock.show();
          }

          if (e.currentTarget.scrollTop > window.innerHeight) {
            $mainPhoneBlock.show();
            $.fn.fullpage.moveTo(2, 0);
          }
        }
      });
    });

    $('#fullpage').fullpage({
      navigation: true,
      responsiveWidth: 760,
      animateAnchor: true,
      normalScrollElements: '.hero-content-wrapper, .bottom-section-content-wrapper',
      scrollingSpeed: 800,
      onLeave: function (index, nextIndex, direction) {
        if (screen.width > 760) {
          $('.phone-content').attr('class', 'phone-content');
          $('.phone-content').addClass(`section_${nextIndex}`);

          if (nextIndex === 5) {
            $('.bottom-section-content-wrapper').addClass('unscrollable')
          }

          setTimeout(function () {
            $('.bottom-section-content-wrapper').removeClass('unscrollable');
          }, 1500);

          if (nextIndex === 1 && direction === "up") {
            $('.hero-content-wrapper').off('scroll');
            $('.overlay').addClass('active')
            setTimeout(function () {
              $('.hero-content-wrapper').scroll(function (event) {
                if (event.currentTarget.scrollTop >= this.offsetHeight) {
                  $('.main-phone-block').show();
                  $('.subhero-phone-block').hide();
                } else {
                  $('.main-phone-block').hide();
                  $('.subhero-phone-block').show();
                }
                
                $('.hero-content-wrapper').on( 'DOMMouseScroll mousewheel', function(event) {
                  if (event.currentTarget.scrollTop > window.innerHeight  && event.originalEvent.wheelDelta < 0) {
                    $('.main-phone-block').show();
                    $.fn.fullpage.moveTo(2, 0); 
                  }
                })
              })
              $('.overlay').removeClass('active')
            }, 700);
          }

          setTimeout(function() {
            if (nextIndex === 5) {
              $('.main-phone-block').hide()
              $('.bottom-phone-block').show()
            }
          }, 500);
          if (nextIndex === 4 && index === 5 ) {
            $('.main-phone-block').show()  
            $('.bottom-phone-block').hide()  
          } 
        }
      }
    });

    $('.owl-carousel').owlCarousel({
      loop: false,
      responsiveClass: true,
      nav: false,
      responsive:{
        0:{
          items:1,
          dots: true
        },
        760:{
          items:3
        }
      }
    });

  });
}

export default app;
