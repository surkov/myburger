import app from './app';
import $ from 'jquery';

document.addEventListener('DOMContentLoaded', () => {
  $('.hero-content-wrapper').addClass('unscrollable');
  $('.overlay').addClass('active');
  app();
});
