exports.files = {
  javascripts: {
    joinTo: {
      'js/app.js': /^(app)/,
      'js/vendor.js': /^node_modules/,
    },
  },
  stylesheets: { joinTo: 'css/app.css' },
};


exports.plugins = {
  babel: { presets: ['latest', 'stage-0'] },
  // pleeease: {
  //   sass: true,
  //   autoprefixer: {
  //     browsers: ['> 1%'],
  //   },
  // },
  // copycat: {
  //   fonts: ['node_modules/bootstrap/dist/fonts'],
  //   onlyChanged: true,
  // },
};

exports.modules = {
  autoRequire: {
    'js/app.js': ['js/initialize'],
  },
};

exports.npm = {
  globals: {
    jQuery: 'jquery',
    $: 'jquery',
  },
  styles: {
    'fullpage.js': ['dist/jquery.fullpage.min.css']
  },
};
